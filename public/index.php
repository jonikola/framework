<?php

/**
 * Front controller
 *
 * PHP version 5.5
 */

require_once '../Core/autoload.php';

/**
 * Routing
 */

$router = new Core\Router();

$router->dispatch($_SERVER['QUERY_STRING']);