<?php

namespace Core;

use PDO;
use App\Config;

/**
 * Base model
 *
 * PHP version 5.5
 */
abstract class Model
{
	/**
	 * Flag that shows if object is newly created
	 * @var boolean
	 */
	private $isNew = false;

	/**
	 * Database table name
	 * @var string
	 */
	protected static $table = '';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->isNew = true;
	}

	/**
	 * Get the PDO database connection
	 *
	 * @return mixed
	 */
	protected static function getDB()
	{
		static $db = null;

		if ($db === null) {
			$dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' .
				Config::DB_NAME . ';charset=utf8';
			$db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);

			// Throw an Exception when an error occurs
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		return $db;
	}

	/**
	 * Check if object is newly created or fetched from databas
	 *
	 * @return boolean
	 */
	protected function isNew()
	{
		return $this->isNew;
	}

	/**
	 * Save or update object in database
	 *
	 * @return void
	 */
	public function save()
	{
		if ($this->isNew()) {
			$this->insert();
		} else {
			$this->update();
		}
	}

	/**
	 * Insert new object into the database
	 *
	 * @return void
	 */
	public function insert() 
	{
		$data = $this->objectToArray($this);
		if (isset($data['id'])) {
			unset($data['id']);
		}

		$fields = array_keys($data);
		$values = array_values($data);

		$sql = "INSERT INTO " . self::getTableName() . " (" . implode(", ", $fields) . ") VALUES (";
		for ($i = 0; $i < count($values); $i++) {
			if ($i !== count($values) - 1) {
				$sql .= ":value" . $i . ", ";
			} else {
				$sql .= ":value" . $i;
			}
		}
		$sql .= ")";

		try {
			$db = static::getDB();
			$stmt = $db->prepare($sql);
			for ($i = 0; $i < count($values); $i++) {
				$stmt->bindParam(":value" . $i, $values[$i]);
			}

			$stmt->execute();
			$id = $db->lastInsertId();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}

		$this->isNew = false;
		$this->id = $id;
		$db = null;
	}

	/**
	 * Update existing object
	 *
	 * @return void
	 */
	public function update() 
	{
		$data = $this->objectToArray($this);
		$id = $data['id'];
		unset($data['id']);

		$fields = array_keys($data);
		$values = array_values($data);

		$sql = "UPDATE " . self::getTableName() . " SET ";
		for ($i = 0; $i < count($fields); $i++) {
			if ($i !== count($fields) - 1) {
				$sql .= $fields[$i] . " = :value" . $i . ", ";
			} else {
				$sql .= $fields[$i] . " = :value" . $i;
			}
		}
		$sql .= " WHERE id = :id";

		try {
			$db = static::getDB();
			$stmt = $db->prepare($sql);
			for ($i = 0; $i < count($values); $i++) {
				$stmt->bindParam(":value" . $i, $values[$i]);
			}
			$stmt->bindParam(":id", $id);

			$stmt->execute();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * Fetch object from database
	 *
	 * @param mixed $data Parameters for selecting object in database
	 *
	 * @return mixed
	 */
	public static function get($data = null) 
	{
		switch ($data) {
			case null:				
				$sql = "SELECT * FROM " . self::getTableName();
				try {
					$db = static::getDB();
					$stmt = $db->query($sql);

					$result = self::arrayToObject($stmt->fetchAll(PDO::FETCH_ASSOC));

					return $result;
				} catch (PDOException $e) {
					echo $e->getMessage();
				}
				break;

			case is_numeric($data):
				$sql = "SELECT * FROM " . self::getTableName() . " WHERE id=:id";
				try {
					$db = static::getDB();
					$stmt = $db->prepare($sql);
					$stmt->bindParam(":id", $data);
					$stmt->execute();
					$result = $stmt->fetchObject();
					$result->isNew = false;

					return $result;
				} catch (PDOException $e) {
					echo $e->getMessage();
				}
				break;

			case is_array($data):
				$sql = "SELECT * FROM " . self::getTableName() . " WHERE ";
				$i = 0;
				$len = count($data);
				foreach ($data as $key => $value) {
					if (++$i < $len) {
						$sql .= "$key='$value' AND ";
					} else {
						$sql .= "$key='$value'";
					}
				}
				try {
					$db = static::getDB();
					$stmt = $db->prepare($sql);
					$stmt->execute();
					$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$result = self::arrayToObject($result);						

					return $result;
				} catch (PDOException $e) {
					echo $e->getMessage();
				}
				break;
		}
	}

	/**
	 * Delete record from database
	 *
	 * @return void
	 */
	public function delete() 
	{
		$sql = "DELETE FROM " . self::getTableName() . " WHERE id = :id";

		try {
			$stmt = static::getDB()->prepare($sql);
			$stmt->bindParam(":id", $this->id);
			$stmt->execute();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * Convert object to arraya
	 *
	 * @param object $object
	 *
	 * @return array
	 */
	private function objectToArray($object) 
	{
		if (!is_object($object))
			return $object;

		$array = [];

		foreach (get_object_vars($object) as $key => $value) {
			$array[$key] = is_object($value) ? $this->objectToArray($value) : $value;
		}
		unset($array['isNew']);
		unset($array['table']);

		return $array;
	}

	/**
	 * Convert array to object
	 * 
	 * @param array $array
	 *
	 * @return object
	 */
	private static function arrayToObject($array)
	{
		$result = [];

		foreach (array_keys($array) as $key) {
			$obj = (object)$array[$key];
			$obj->isNew = false;
			array_push($result, $obj);
		}

		return $result;
	}

	/**
	 * Get database table name
	 *
	 * @return string
	 */
	private static function getTableName()
	{
		if (empty(self::$table)) {
			$name = explode("\\", get_called_class());//get_class($this));
			$name = strtolower(array_pop($name)) . 's';
		} else {
			$name = self::$table;
		}

		return $name;
	}
}